<?php

session_start();
if(isset($_GET['logout']) && $_GET['logout'] == 1){
  if(isset($_SESSION['logowanie']))  $_SESSION['logowanie'] == 'false';
  session_destroy();
}




?>


<!doctype HTML>
<html>
<head>
  <meta charset="utf-8">
  <link href="https://fonts.googleapis.com/css?family=Bree+Serif" rel="stylesheet">
<style>
@import url("style/style.css");
</style>
</head>
<body>
<div id="shadow2">
  <div id="modal3">
    <span id="exit" onclick="exit()">X</span>
    <p id="error_tresc">Tekst błędu.</p>
  </div>
</div>

<nav id="menubar">
  <div class='text'>menu</div>
  <div class="menuoption"><a href="index.php">Strona główna</a></div>
  <div class="menuoption"><a href="sklep.php">Sklep</a></div>
  <div class="menuoption"><a href="kontekst.html">Kontakt</a></div>
  <div class="menuoption" id="userico" onclick="location.href='myprofile.php'">&nbsp;</div>
  <div class="menuoption" id="wyloguj"><a href="logowanie.php?logout=1">Wyloguj</a></div>


</nav>
<div class="banner"></div>
<aside>
  <div class='center'>
<h1>Zaloguj się</h1>
<div class="form form2">
<form method="post" action="zaloguj.php" id="formularz">
<table class="logowanie">
<tr><td>Login:</td><td><input type="text" name="login"></td></tr>
<tr><td>Hasło:</td><td><input type="password" name="paswd"></td></tr>
</table>
<input type="button" onclick="sprawdzform()" value="Zaloguj"><span id="baddata">Złe dane</span>
</form>
</div>
<h1 id="zar">Zarejestruj się</h1>
<div class="form form2">
<form method="post" action="zarejestruj.php" id="formularz2">
<table class="logowanie rejestracja">
  <tr><td>Imię:</td><td><input onclick="clearerror(0)" type="text" name="fname"><span class='error'>X</span></td></tr>
  <tr><td>Nazwisko:</td><td><input onclick="clearerror(1)" type="text" name="sname"><span class='error'>X</span></td></tr>
<tr><td>Login:</td><td><input onclick="clearerror(2)" type="text" name="login"><span class='error'>X</span></td></tr>
<tr><td>Email:</td><td><input onclick="clearerror(3)" type="text" name="email"><span class='error'>X</span></td></tr>
<tr><td>Telefon:</td><td><input onclick="clearerror(4)" type="text" name="tel"><span class='error'>X</span></td></tr>
<tr><td>Hasło:</td><td><input onclick="clearerror(5)" type="password" name="paswd1"><span class='error'>X</span></td></tr>
<tr><td>Powtórz hasło:</td><td><input onclick="clearerror(6)" type="password" name="paswd2"><span class='error'>X</span></td></tr>
</table>
<input type="button" onclick="sprawdzformrej()" value="Zarejestruj">
</form>
</div>
</div>
</aside>


<script type="text/javascript">
<?php
if(isset($_SESSION['logowanie']) AND $_SESSION['logowanie'] == 'true'){
echo "console.log('zalogowano');";
echo "document.getElementById('wyloguj').style.display = 'inline-block';";
echo "document.getElementById('userico').style.display = 'inline-block';";
}
else if(isset($_SESSION['logowanie']) AND $_SESSION['logowanie'] == 'fail' ){
  echo "document.getElementById('wyloguj').style.display = 'none';";
  echo "document.getElementById('userico').style.display = 'none';";
}
else{
  echo "document.getElementById('wyloguj').style.display = 'none';";
  echo "document.getElementById('userico').style.display = 'none';";
}




 ?>




function sprawdzform(){
  if(document.getElementsByName("login")[0].value != "" && document.getElementsByName("paswd")[0].value != "" ){
    document.getElementById("formularz").submit();
  }else{
    if(document.getElementsByName("login")[0].value == ""){document.getElementsByName("login")[0].style.animationName = "animacja" ;
    var elm = document.getElementsByName("login")[0];
    var newone = elm.cloneNode(true);
    elm.parentNode.replaceChild(newone, elm);
}
    if(document.getElementsByName("paswd")[0].value == ""){document.getElementsByName("paswd")[0].style.animationName = "animacja" ;
    elm = document.getElementsByName("paswd")[0];
    newone = elm.cloneNode(true);
    elm.parentNode.replaceChild(newone, elm);
  }
  };
}

  function sprawdzformrej(){
      var war = true;
      var reg =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(document.getElementsByName("login")[1].value == ""){
          war = false;
          document.getElementsByClassName("error")[2].style.display = "inline-block";
        }
        if(document.getElementsByName("tel")[0].value == ""){
          war = false;
          document.getElementsByClassName("error")[4].style.display = "inline-block";
        }
        if(document.getElementsByName("paswd1")[0].value == ""){
          war = false;
          document.getElementsByClassName("error")[5].style.display = "inline-block";
        }
        if(document.getElementsByName("paswd2")[0].value == "" || document.getElementsByName("paswd2")[0].value != document.getElementsByName("paswd1")[0].value ){
          war = false;
          document.getElementsByClassName("error")[6].style.display = "inline-block";
        }
        if(document.getElementsByName("email")[0].value == "" || !reg.test(document.getElementsByName("email")[0].value) ){
          war = false;
          document.getElementsByClassName("error")[3].style.display = "inline-block";
        if(document.getElementsByName("fname")[0].value == ""){
          war = false;
          document.getElementsByClassName("error")[0].style.display = "inline-block";
        }
        if(document.getElementsByName("sname")[0].value == ""){
          war = false;
          document.getElementsByClassName("error")[1].style.display = "inline-block";
        }
      }
      if(war){
        document.getElementById("formularz2").submit();
      }
}
  function clearerror(i){
    document.getElementsByClassName("error")[i].style.display = "none";
  }
function exit(){document.getElementById("shadow2").style.display='none';}

<?php
if(isset($_GET['err'])){
  echo 'document.getElementById("shadow2").style.display = "block";';
  $err = $_GET['err'];
  switch($err){
    case 0:
      echo "document.getElementById('error_tresc').innerHTML = 'Pomyślnie zarejestrowano użytkownika';";
      break;
    case 1:
      echo "document.getElementById('error_tresc').innerHTML = 'Użytkownik o takim loginie już istnieje.';";
      break;
    case 2:
      echo "document.getElementById('error_tresc').innerHTML = 'Użytkownik o takim adresie email już istnieje.';";
      break;
    case 3:
      echo "document.getElementById('error_tresc').innerHTML = 'Użytkownik o takim loginie i adresie email już istnieje.';";
      break;
    case 4:
      echo "document.getElementById('error_tresc').innerHTML = 'Nie udało się zarejestrować użytkownika';";
      break;
    case 5:
      echo "document.getElementById('error_tresc').innerHTML = 'Błędny login lub hasło';";
      break;
  }

}

?>
</script>
</body>
</html>
